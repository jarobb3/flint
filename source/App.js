/**
	Define and instantiate your enyo.Application kind in this file.  Note,
	application rendering should be deferred until DOM is ready by wrapping
	it in a call to enyo.ready().
*/

enyo.kind({
	name: "flintalpha.Application",
	kind: "enyo.Application",
	view: "flintalpha.MainView",
	
	components: [
         { tag : "div", components: [
             { tag: "span", content: "hi" }
         ]}
    ]
});

enyo.ready(function () {
	new flintalpha.Application({name: "app"});
});