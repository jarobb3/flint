import os
import json
import urllib

import jinja2
import webapp2

import logging
import urllib2




JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class MainPage(webapp2.RequestHandler):

    def get(self):
        response = urllib2.urlopen('http://api.rottentomatoes.com/api/public/v1.0/lists/movies/upcoming.json?apikey=yx48ce2xdtbmu8m3zxuqdp6q')
        response_str = response.read()
        response_json = json.loads( response_str )
        response_movies = response_json['movies']
        #total_movies = response_json['total']
        #logging.info('hi: ' + str(total_movies))
        
        #posters_list = []
        #movie_names = []
        movies = []
        for i in range(15):
            movies.append({'poster' : response_movies[i]['posters']['detailed'], 
                           'title' : response_movies[i]['title'],
                           'cast' : response_movies[i]['abridged_cast']
            })
            #posters_list.append(response_movies[i]['posters']['detailed'])
            #movie_names.append(response_movies[i]['title'])
            #output_str += '<img src = ' + response_movies[i]['posters']['profile'] + '>'
        
        
        
        #first_movie = response_json['movies'][0]
        #first_movie_poster = first_movie['posters']['detailed']
        
        #self.response.write('<img src =' + first_movie_poster + '>')\
        
        template_values = { 'movies' : movies }
        #self.response.write(output_str)
        
        template = JINJA_ENVIRONMENT.get_template('single-page.html')
        self.response.write(template.render(template_values))


application = webapp2.WSGIApplication([
    ('/', MainPage),
], debug=True)
